#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "parse-settings.h"


char command[512];

int main(int argc, char **argv)
{
	char *pbffile;

	if(argc != 2){
		fprintf(stderr,"Usage: osm-tile-server-import <pbf-file>\n");
		return -1;
	}

	pbffile = (char*)malloc(256);

	int i,p=0;
	int pbflen = strlen(argv[1]);
	for(i=0;i<pbflen;i++){
		if(argv[1][i] == '\'') {
			pbffile[p] = '\'';
			p++;
			pbffile[p] = '\\';
			p++;
			pbffile[p] = '\'';
			p++;
			pbffile[p] = '\'';
		}
		else{
			pbffile[p] = argv[1][i];
		}
		p++;
		if(p >= 255) break;
	}
	pbffile[p] = 0;
	printf("pbffile '%s'",pbffile);

	parse_settings();

	if(strcmp(cfg_slim,"true") == 0) {
		snprintf(cfg_slim,10,"--slim");
	}
	else {
		cfg_slim[0] = 0;
	}
	snprintf(command,255,"osm2pgsql %s -d %s --cache-strategy %s -C %s --number-processes %s --hstore -S /usr/share/osm2pgsql/default.style '%s'",cfg_slim,cfg_dbname,cfg_cache_strategy,cfg_memory,cfg_processes,pbffile);
	printf("Importing to database with this command:\n\n");
	printf(" %s\n\n",command);

	free(pbffile);

	return system(command);

}

